maintainer       "Joe Balancio"
maintainer_email "jlbalancio@gmail.com"
license          ""
description      "Installs/Configures nodejs-binary"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.1.0"
