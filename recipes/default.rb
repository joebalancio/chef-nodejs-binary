#
# Cookbook Name:: nodejs-binary
# Recipe:: default
#
# Copyright 2012, Joe Balancio
#

nodeVersion = node['nodejs_binary']['version']

baseUri = "http://nodejs.org/dist/v#{nodeVersion}/"

case node['os']
when 'linux', 'darwin', 'sunos'
  target = 'bin include lib share'
  if node['kernel']['machine'] == 'x86_64'
    machine = 'x64'
  else
    machine = 'x86'
  end
  name = "node-v#{nodeVersion}-#{node['os']}-#{machine}"
  file = name + ".tar.gz"
when 'windows'
  if node['kernel']['machine'] == ''
    baseUri += 'x64/'
  end
  file = 'node.exe'
end

remote_file "#{Chef::Config[:file_cache_path]}/#{file}" do
  source baseUri + file
  not_if { File.exists? "#{Chef::Config[:file_cache_path]}/#{file}" }
end

case node['os']
when 'linux', 'darwin', 'sunos'
  execute "install nodejs binary" do
    cwd Chef::Config[:file_cache_path]
    command <<-EOH
      (mkdir #{name} && tar -zxf #{file} -C #{name} --strip-components 1)
      (cd #{name} && cp -rf #{target} #{node[:nodejs_binary][:prefix]})
    EOH
    not_if "node -v | grep #{node[:nodejs_binary][:version]}"
  end
when 'windows'
  log 'I do not know how to deal with windows yet' do
    level :warn
  end
end
